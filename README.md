# XFW Hello World #

### Description ###
This is a simple example of modification for World of Tanks based on XFW (XVM Framework).

If you have any further questions feel free to ask on KoreanRandom's forums in [development section (you can ask in english or russian)](http://www.koreanrandom.com/forum/forum/56-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-development/)

***

### Описание ###
Это простой пример модификации для World of Tanks на основе XFW (XVM Framework).

Если у вас есть какие-то вопросы, обращайтесь на форумы KoreanRandom в [раздел разработки](http://www.koreanrandom.com/forum/forum/56-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-development/).


### Details and updates 07 февраля 2017  by https://koreanrandom.com/forum/user/40874-john-nash/ ###

	Пара моментов, очевидных для опытных разрабов, однако полезных для новичка.

	1. Поскольку мод суть надстройка над XFW,  для его работы, очевидно. нужно, чтобы в клиенте игры XFW был уже установлен. Поскольку отдельного дистрибутива XFW (котррый можно было бы скопировать в ппку res_mods) не существует, можно установить весь XVM целиком. Громоздко. ла, но для тестирования сойдёт.

	2. Проект использует библиотеки XFW. Они собраны в папке swc проекта. Это копии файлов из сборки XFW. Для совместимости необходимо, чтобы эти библиотеки были теми же самыми, что и в версии XFW, развёрнутой в клиенте. В скомпилированном проекте XFW эти файлы лежат в папке ~output\swc. В дистрибутиве XVM этих файлов нет. Брать их следует из проекта XFW, который компилится для установленного на клиент XVM-а. То есть из папки вида %XVM%\src\xfw\~output\swc

	3. На момент написания (07 февраля 2017) актуальна версия WoT 9.17.0.3 и XVM-6.5.4-RELEASE. Файлами из последнего релиза и заменеы исходные файлы библиотек .swc.

	4. Реплей для теста также заменён на совместимый с текущей версией игры.

	5. Проект компилируется в единственный файл bin\xfw_hello_world.swf. Деплоймент сводится к его копированию в папку %WOT_DIR%\res_mods\mods\packages\xfw_hello_world\as_battle\ (где WOT_DIR -- папка с клиентом игры).

	6. Если мод работает, то при заходе в бой в центре экрана появляется лого XVM и надпись красными буквами под ним.